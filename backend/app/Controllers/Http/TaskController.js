'use strict'

const Task = use('App/Models/Task')

class TaskController {

  async store ({ request, response, params }) {
    try {
      const { users, ...data } = request.only(['title', 'description', 'priority', 'status', 'date_start', 'date_end', 'users'])

      /* const { title, description, priority, status, date_start, date_end, users } = request.post() */

      const task = await Task.create({ title, description, priority, status, date_start, date_end, project_id: params.projects_id})

      if (users && users.length > 0) {
        await task.users().attach(users)
        task.users = await task.users().fetch()
      }

      /* return task */
      response.status(201).json({
        message: 'Successfully created a new task!',
        data: task
      })
    } catch (err) {
      return response
        .status(err.status)
        .send({error:{ message: 'Algo não deu certo, tente novamente adicionar a tarefa!'}})
    }
  }

  async show ({ params }) {
    const task = await Task.findOrFail(params.id)

    return task
  }

  async index ({ params }) {
    const tasks = await Task.query()
      .where('project_id', params.projects_id)
      .with('project')
      .fetch()

    return tasks
  }

  async update ({ params, request, response }) {
    const task = await Task.findOrFail(params.id)

    const data = request.only(['title', 'description', 'priority', 'status', 'date_start', 'date_end', 'file_id'])

    task.merge(data)

    await task.save()

    return task
  }

  async destroy ({ params, request, response }) {
    const task = await Task.findOrFail(params.id)

    await task.delete()
  }
}

module.exports = TaskController
