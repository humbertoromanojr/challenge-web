'use strict'

const Project = use('App/Models/Project')

class ProjectController {

  async index ({ request, response, view }) {
    const projects = await Project.query()
      .with('users', 'client')
      .fetch()

    return projects
  }

  async store ({ request, response, auth }) {
    const data = request.only(['title', 'description', 'price', 'status', 'date_start', 'date_end', 'client_id', 'user_id'])

    const project = await Project.create({ ...data, user_id: auth.user.id })

    return project
  }

  async show ({ params }) {
    const project = await Project.findOrFail(params.id)

    await project.load('users')
    await project.load('tasks')
    await project.load('client')

    return project
  }

  async update ({ params, request }) {
    const project = await Project.findOrFail(params.id)
    const data = request.only(['title', 'description', 'price', 'status', 'date_start', 'date_end', 'client_id', 'user_id'])

    project.merge(data)

    await project.save()

    return project
  }

  async destroy ({ params }) {
    const project = await Project.findOrFail(params.id)

    await project.delete()
  }
}

module.exports = ProjectController
