'use strict'

const Client = use('App/Models/Client')

class ClientController {

  async index ({ request, response, view }) {
    const clients = await Client.query()
      .with('file')
      .fetch()

    return clients
  }

  async store ({ request }) {
    const data = request.only(['title', 'logo', 'cnpj_cpf', 'email', 'phone', 'address'])

    const client = await Client.create(data)

    return client
  }

  async show ({ params }) {
    const client = await Client.findOrFail(params.id)

    await client.load('file')

    return client
  }

  async update ({ params, request }) {
    const client = await Client.findOrFail(params.id)
    const data = request.only(['title', 'logo', 'cnpj_cpf', 'email', 'phone', 'address'])

    client.merge(data)

    await client.save()

    return client
  }

  async destroy ({ params }) {
    const client = await Client.findOrFail(params.id)

    await client.delete()
  }
}

module.exports = ClientController
