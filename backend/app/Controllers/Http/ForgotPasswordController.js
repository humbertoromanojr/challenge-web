'use strict'

const crypto = require('crypto')
const User = use('App/Models/User')
const Mail = use('Mail')
var moment = require('moment');

class ForgotPasswordController {
  async store({ request, response }) {
    try {
      const email = request.input('email')
    const user = await User.findByOrFail('email', email)

    user.token = crypto.randomBytes(16).toString('hex')
    user.token_created_at = new Date()

    await user.save()

    await Mail.send(
      ['emails.forgot_password', 'emails.forgot_password-text'],
      {
        email,
        token: user.token,
        link: `${request.input('redirect_url')}?token=${user.token}`
      },
      message => {
        message.to(user.email)
        .from('admin@tecnovix.com.br', 'Admin | Tecnovix')
        .subject('Forgot Password')
      }
    )

    } catch (err) {
      return response
      .status(err.status)
      .send({error: { message: 'Algo não deu certo, esse email existe? ou já está em uso!' }})
    }
  }

  async update ({ request, response }) {
    try {
      const { token, password } = request.all()

      const user = await User.findByOrFail('token', token)

      const tokenExpired = moment()
        .subtract('2', 'days')
        .isAfter(user.token_created_at)

      if (tokenExpired) {
        return response
        .status(401)
        .send({error: { message: 'O Token de recuperação está expirado!' }})
      }

      user.token = null
      user.token_created_at = null
      user.password = password

      await user.save()

    } catch (err) {
      return response
        .status(err.status)
        .send({error: { message: 'Algo não deu certo, tentar resetar sua senha novamente!' }})
    }
  }


}// end class

module.exports = ForgotPasswordController
