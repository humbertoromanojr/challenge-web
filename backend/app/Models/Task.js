'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Task extends Model {
  project () { return this.belongsTo('App/Models/Project') }

  users () {
    return this
    .belongsToMany('App/Models/User')
    .pivotTable('task_user')
  }

  file () { return this.belongsTo('App/Models/File') }
}

module.exports = Task
