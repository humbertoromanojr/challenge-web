'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TaskUserSchema extends Schema {
  up () {
    this.create('task_user', (table) => {
      table
        .integer('task_id')
        .unsigned()
        .index('task_id')
      table
        .foreign('task_id')
        .references('tasks.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .integer('user_id')
        .unsigned()
        .index('user_id')
      table
        .foreign('user_id')
        .references('users.id')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
  }

  down () {
    this.drop('task_user')
  }
}

module.exports = TaskUserSchema
