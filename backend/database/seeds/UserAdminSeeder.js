'use strict'

const User = use('App/Models/User')

class UserAdminSeeder {
  async run () {

    await User.create({
      username: 'Administrator',
      email: 'admin@tecnovix.com',
      password: '123456',
    })

  }
}

module.exports = UserAdminSeeder
