'use strict'

const Route = use('Route')

Route.post('sessions', 'SessionController.store')

Route.post('users', 'UserController.store')
Route.get('users', 'UserController.index')

Route.post('forgotpasswords', 'ForgotPasswordController.store')
Route.put('forgotpasswords', 'ForgotPasswordController.update')

Route.get('/files/:id', 'FileController.show')

Route.group(() => {

  Route.post('/files', 'FileController.store')

  Route.resource('projects', 'ProjectController').apiOnly()

  Route.resource('projects.tasks', 'TaskController').apiOnly()

  Route.resource('clients', 'ClientController').apiOnly()

}).middleware(['auth'])


