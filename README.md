# Challenge Tecnovix

<h1 align="center">
<img src="https://www.tecnovix.com.br/wp-content/uploads/2020/03/logo-tecnovix.svg">
</h1>


## Backend

- Git [Git](https://git-scm.com)
- Node.js [use NVM v.0.34.0 - Node.js v12.16.3 - NPM v.6.14.4](https://nodejs.org)
- Yarn [Yarn v1.22.4](https://yarnpkg.com)
- Mysql [MySql 8.0.20 ](https://www.mysql.com)
- DBeaver Community [DBeaver Community v.7.0.3](https://dbeaver.com)
- Insomnia [Insomnia v.7.1.1](https://insomnia.rest)
- MailTrap Testing [MailTrap](https://mailtrap.io)


## Frontend



## :memo: License

This project is under the MIT license. See the [LICENSE](LICENSE)  file for more details.

---

## Demonstration

<h2 align="center">
</h2>
