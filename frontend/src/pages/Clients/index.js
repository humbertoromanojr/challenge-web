import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { FaPlus } from 'react-icons/fa';

import NotFound from '~/components/NotFound';
import Modal from '~/components/Modal';
import api from '~/services/api';

import { Container, Content, Header } from './styles';
import { deleteClientRequest, getClientsRequest } from '~/store/modules/clients/actions';

export default function Clients() {
  const dispatch = useDispatch();

  const [openModal, setOpenModal] = useState(false);
  const [clients, setClients] = useState([]);
  const [selectClient, setSelectClient] = useState(null);

  useEffect(() => {
    async function getAllClients() {
      const response = await api.get('clients');

      const data = response.data.map(clients => ({
        ...clients,
      }));

      setClients(data);
    }

    getAllClients();
  }, []);

  useEffect(() => {
    dispatch(getClientsRequest());
  }, [dispatch]);

  useEffect(() => {
    setOpenModal(false);
  }, [clients]);

  function closeModalForm() {
    setOpenModal(false);
  }

  function openModalForm(clients) {
    setSelectClient(clients);
    setOpenModal(true);
  }

  function handleSubmit() {
    const { id } = selectClient;

    dispatch(deleteClientRequest(id));

    setOpenModal(false);
  }

  return (
    <Container>
      <Header>
        <h1>Gerenciando Clientes</h1>
        <div>
          <Link to="/clients/new">
            <FaPlus size={14} color="#fff" />
            <label>CADASTRAR</label>
          </Link>
        </div>
      </Header>
      <Content>
        <section>
          {clients.length > 0 ? (
            <table>
              <thead>
                <tr>
                  <th width="2%">#</th>
                  <th width="10%">Cliente</th>
                  <th width="10%">CNPJ/CPF</th>
                  <th width="40%">ENDEREÇO</th>
                  <th width="15%">EMAIL</th>
                  <th width="10%" align="center">TELEFONE</th>
                  <th width="13%">&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                {clients.map(client => (
                  <tr key={client.id}>
                    <td>{client.logo}</td>
                    <td>{client.title}</td>
                    <td>
                      {client.cnpj_cpf}
                    </td>
                    <td>{client.address}</td>
                    <td>{client.email}</td>
                    <td align="center">{client.phone}</td>
                    <td>
                      <Link to={`/clientsform/${client.id}/edit`}>editar</Link>
                      <button type="button" onClick={() => openModalForm(clients)}>
                        apagar
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          ) : (
            <NotFound />
          )}
        </section>
      </Content>

      {openModal && (
        <Modal>
          <p>
            Deseja deletar este Cliente: <strong>{selectClient.title}</strong>
          </p>
          <button type="button" onClick={handleSubmit}>
            Confirmar
          </button>
          <button type="button" onClick={closeModalForm}>
            Cancelar
          </button>
        </Modal>
      )}
    </Container>
  );
}
