import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { FaChevronLeft, FaCheck } from 'react-icons/fa';
import { Input, Form } from '@rocketseat/unform';
import * as Yup from 'yup';

import api from '~/services/api';

import {
  createProjectRequest,
  updateProjectRequest,
} from '~/store/modules/projects/actions';
import ContentHeader from '~/components/ContentHeader';
import { Container, Content, ContentForm } from './styles';

const schema = Yup.object().shape({
  title: Yup.string().required('Nome do projeto Obrigatório'),
  client_id: Yup.number().required('Número de id do cliente é Obrigatório'),
  price: Yup.string().required('Valor do projeto é Obrigatório'),
});

export default function ProjectsForm({ match }) {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const [project, setProject] = useState(null);

  const list = [
    {id: 0, name: 'Aprovado'},
    {id: 1, name: 'Ativo'},
    {id: 2, name: 'Em Progresso'},
    {id: 3, name: 'Atrasado'},
    {id: 4, name: 'Cancelado'},
    {id: 5, name: 'Completo'},
  ];

  useEffect(() => {
    if (!match.params.id) return;

    async function getProject(id) {
      setLoading(true);

      const response = await api.get(`projects/${parseInt(id, 10)}`);

      setProject(response.data);

      setLoading(false);
    }

    getProject(match.params.id);
  }, [match.params.id]);

  const headerTitle = useMemo(() => {
    return match.params.id ? 'Edição de Projeto' : 'Cadastro de Projeto';
  }, [match.params.id]);

  function handleCreate(data, { resetForm }) {
    dispatch(createProjectRequest(data));
    resetForm();
    console.log('ProjectForm -> create -> ', data)
  }

  function handleUpdate(data) {
    const { id } = match.params;

    dispatch(updateProjectRequest(id, data));
  }

  if (loading) {
    return <div />;
  }

  return (
    <Container>
      <Form
        /* schema={schema} */
        initialData={project}
        onSubmit={match.params.id ? handleUpdate : handleCreate}
      >
        <ContentHeader title={headerTitle}>
          <Link to="/dashboard">
            <FaChevronLeft size={14} color="#fff" />
            <label>voltar</label>
          </Link>

          <button type="submit">
            <FaCheck size={14} color="#fff" />
            <label>salvar</label>
          </button>
        </ContentHeader>

        <Content>
          <section>
            <ContentForm>
              <div>
                <label>Projeto</label>
                <Input name="title" placeholder="Nome do projeto" />
              </div>

              <div>
                <label>Cliente</label>
                <Input name="client_id"/>
              </div>

            </ContentForm>

            <ContentForm>
              <div>
                <label>Valor</label>
                <Input name="price" />
              </div>

              <div>
                <label>Status</label>
                {match.params.id ? (
                  <select name="status">
                    {
                      list.map((status, index) => (
                      <option key={index} value={status.id}>{status.name}</option>
                      ))
                    }
                  </select>
                ) : (
                  <select name="status">
                    <option value="">Selecione</option>
                    {
                      list.map((status, index) => (
                      <option key={index} value={status.id}>{status.name}</option>
                      ))
                    }
                  </select>
                )
                } 
              </div>

              <div>
                <label>Data inícial</label>
                <Input name="date_start" />
              </div>

              <div>
                <label>Data conclusão</label>
                <Input name="date_end" />
              </div>
            </ContentForm>
           
            <ContentForm>
              <div>
                <label>Descrição</label>
                <Input name="description" multiline placeholder="Sobre o projeto" />
              </div>
            </ContentForm>
            
          </section>
        </Content>
      </Form>
    </Container>
  );
}

ProjectsForm.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
};
