import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { FaChevronLeft, FaCheck } from 'react-icons/fa';
import { Input, Form } from '@rocketseat/unform';
import * as Yup from 'yup';

import api from '~/services/api';

import {
  createClientRequest,
  updateClientRequest,
} from '~/store/modules/clients/actions';
import ContentHeader from '~/components/ContentHeader';
import { Container, Content, ContentForm, Logo } from './styles';

const schema = Yup.object().shape({
  title: Yup.string().required('Nome da empresa Obrigatório'),
  email: Yup.string()
    .email()
    .required('Email Obrigatório'),
  cnpj_cpf: Yup.string().required('CNPJ ou CPF é Obrigatório'),
  address: Yup.string().required('Endereço Obrigatorio'),
  phone: Yup.string().required('Telefone de contato Obrigatória'),
});

export default function ClientsForm({ match }) {
  const dispatch = useDispatch();
  const [client, setClient] = useState([]);

  const [loading, setLoading] = useState(false);
  
  const [cnpj_cpf, setCnpj_cpf] = useState(null);
  const [phone, setPhone] = useState(null);
  const [email, setEmail] = useState(null);
  const [address, setAddress] = useState(null);

  useEffect(() => {
    if (!match.params.id) return;

    async function getClient(id) {
      setLoading(true);

      const response = await api.get(`clients/${parseInt(id, 10)}`);

      setClient(response.data);

      setLoading(false);
    }

    getClient(match.params.id);

  }, [match.params.id]);

  const headerTitle = useMemo(() => {
    return match.params.id ? 'Edição de cliente' : 'Cadastro de cliente';
  }, [match.params.id]);

  function handleCreate(data, { resetForm }) {
    dispatch(createClientRequest(data));
    resetForm();
  }

  function handleUpdate(data) {
    const { id } = match.params;

    dispatch(updateClientRequest(id, data));

  }

  if (loading) {
    return <div />;
  }

  return (
    <Container>
      <Form
        schema={schema}
        initialData={client}
        onSubmit={match.params.id ? handleUpdate : handleCreate}
      >
        <ContentHeader title={headerTitle}>
          <Link to="/dashboard">
            <FaChevronLeft size={14} color="#fff" />
            <label>voltar</label>
          </Link>

          <button type="submit">
            <FaCheck size={14} color="#fff" />
            <label>salvar</label>
          </button>
        </ContentHeader>

        <Content>
          <section>
            {match.params.id && (
              <Logo>
                <img src="#" alt="LogoMarca" />
              </Logo>
            )}
           

            <label>Nome Cliente</label>
            <Input name="title" placeholder="Nome ou Empresa" />

            <label>Endereço</label>
            <Input name="address" placeholder="Endereço completo" value={address}
                  onChange={value => setAddress(value)} />

            <ContentForm>
              <div>
                <label>CNPJ/CPF</label>
                <Input
                  name="cnpj_cpf"
                  value={cnpj_cpf}
                  onChange={value => setCnpj_cpf(value)}
                />
              </div>

              <div>
                <label>Telefone</label>
                <Input 
                  name="phone" 
                  value={phone}
                  onChange={value => setPhone(value)}
                />
              </div>

              <div>
              <label>Email</label>
              <Input 
              name="email" 
              placeholder="exemplo@email.com" 
              value={email}
                  onChange={value => setEmail(value)} />
              </div>
            </ContentForm>
          </section>
        </Content>
      </Form>
    </Container>
  );
}

ClientsForm.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
};
