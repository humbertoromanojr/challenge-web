import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { FaChevronLeft, FaCheck } from 'react-icons/fa';
import { Input, Form } from '@rocketseat/unform';
import * as Yup from 'yup';

import api from '~/services/api';

import {
  createEmployeeRequest,
  updateEmployeeRequest,
} from '~/store/modules/employees/actions';
import ContentHeader from '~/components/ContentHeader';
import { Container, Content } from './styles';

const schema = Yup.object().shape({
  title: Yup.string().required('Nome da empresa Obrigatório'),
  email: Yup.string()
    .email()
    .required('Email Obrigatório'),
  cnpj_cpf: Yup.string().required('CNPJ ou CPF é Obrigatório'),
  address: Yup.string().required('Endereço Obrigatorio'),
  phone: Yup.string().required('Telefone de contato Obrigatória'),
});

export default function EmployeesForm({ match }) {
  const dispatch = useDispatch();
  const [employee, setEmployee] = useState([]);

  const [loading, setLoading] = useState(false);
  
  const [username, setUsername] = useState(null);
  const [email, setEmail] = useState(null);

  useEffect(() => {
    if (!match.params.id) return;

    async function getEmployee(id) {
      setLoading(true);

      const response = await api.get(`users/${parseInt(id, 10)}`);

      setEmployee(response.data);

      setLoading(false);
    }

    getEmployee(match.params.id);

  }, [match.params.id]);

  const headerTitle = useMemo(() => {
    return match.params.id ? 'Edição de colaboradores' : 'Cadastro de colaboradores';
  }, [match.params.id]);

  function handleCreate(data, { resetForm }) {
    dispatch(createEmployeeRequest(data));
    resetForm();
  }

  function handleUpdate(data) {
    const { id } = match.params;

    dispatch(updateEmployeeRequest(id, data));

  }

  if (loading) {
    return <div />;
  }

  return (
    <Container>
      <Form
        schema={schema}
        initialData={employee}
        onSubmit={match.params.id ? handleUpdate : handleCreate}
      >
        <ContentHeader title={headerTitle}>
          <Link to="/dashboard">
            <FaChevronLeft size={14} color="#fff" />
            <label>voltar</label>
          </Link>

          <button type="submit">
            <FaCheck size={14} color="#fff" />
            <label>salvar</label>
          </button>
        </ContentHeader>

        <Content>
          <section>
            <label>Nome</label>
            <Input 
              name="username" 
              placeholder="Nome do(a) colaborador(a)"
              value={username}
              onChange={value => setUsername(value)} 
            />

            <label>Email</label>
            <Input 
              name="email" 
              placeholder="exemplo@email.com" 
              value={email}
              onChange={value => setEmail(value)} 
            />
          </section>
        </Content>
      </Form>
    </Container>
  );
}

EmployeesForm.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }).isRequired,
};
