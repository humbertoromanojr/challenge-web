import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { FaPlus } from 'react-icons/fa';

import NotFound from '~/components/NotFound';
import Modal from '../../components/Modal';
import Loading from '~/components/Loading';

import { Container, Content, Header } from './styles';

import {
  getTasksRequest,
  getProjectTasksRequest,
  deleteTaskRequest,
} from '~/store/modules/tasks/actions';

function Dashboard() {
  const tasks = useSelector(state => state.tasks.data);
  const loading = useSelector(state => state.tasks.loading);
  const dispatch = useDispatch();

  const [openModal, setOpenModal] = useState(false);

  const [selectTask, setSelectTask] = useState(null);

  useEffect(() => {
    dispatch(getTasksRequest());
    dispatch(getProjectTasksRequest());
  }, [dispatch]);

  useEffect(() => {
    setOpenModal(false);
  }, [tasks]);

  function handleCloseModal() {
    setOpenModal(false);
  }

  function handleOpenModal(task) {
    setSelectTask(task);
    setOpenModal(true);
  }

  function handleSubmit() {
    const { id } = selectTask;
    dispatch(deleteTaskRequest(id));
  }

  return (
    <Container>
      <Header>
        <h1>Gerenciando Tarefas</h1>
        <div>
          <Link to="/tasks/new">
            <FaPlus size={14} color="#fff" />
            <label>CADASTRAR</label>
          </Link>
        </div>
      </Header>

      {loading ? (
        <Loading>Carregando...</Loading>
      ) : (
        <Content>
          <section>
            {tasks.length === 0 ? (
              <NotFound />
            ) : (
              <table>
                <thead>
                  <tr>
                    <th width="10%">Projeto ID</th>
                    <th width="5%">Aquivo</th>
                    <th width="5%">Tarefas</th>
                    <th width="35%">Descrição</th>
                    <th width="5%">Prioridade</th>
                    <th width="10%" align="center">Status</th>
                    <th width="10%">Data Início/Fim</th>
                    <th width="20%">&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  {tasks.map(task => (
                    <tr key={task.id}>
                      <td>{task.title}</td>
                      <td></td>
                      <td></td>
                      <td>{task.description}</td>
                      <td>{task.priority}</td>
                      <td align="center">{task.status}</td>
                      <td>{task.date_start} - {task.date_end}</td>
                      <td>
                        <Link to={`/tasks/${task.id}/edit`}>editar</Link>
                        <button
                          type="button"
                          onClick={() => handleOpenModal(task)}
                        >
                          apagar
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            )}
          </section>
        </Content>
      )}
      {/* end loading */}
      {openModal && (
        <Modal>
          <p>
            Deseja deletar o Tarefa: <strong>{selectTask.title}</strong>
          </p>
          <button type="button" onClick={handleSubmit}>
            Confirmar
          </button>
          <button type="button" onClick={handleCloseModal}>
            Cancelar
          </button>
        </Modal>
      )}
    </Container>
  );
}

export default Dashboard;
