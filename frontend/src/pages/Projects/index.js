import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { FaSearch, FaPlus } from 'react-icons/fa';
import { Input } from '@rocketseat/unform';
import { FaEdit, FaTrash } from 'react-icons/fa';

import { parseISO, format } from 'date-fns';
import { ptBR } from 'date-fns/locale';
import api from '~/services/api';

import NotFound from '~/components/NotFound';
import Modal from '../../components/Modal';
import Loading from '~/components/Loading';

import { Container, Content, Header, InputSearch } from './styles';

import {
  getProjectsRequest,
  deleteProjectRequest,
} from '~/store/modules/projects/actions';

function Dashboard() {
  // const projects = useSelector(state => state.projects.data);
  const loading = useSelector(state => state.projects.loading);
  const dispatch = useDispatch();

  const [projects, setProjects] = useState([]);
  const [search, setSearch] = useState('');
  const [openModal, setOpenModal] = useState(false);

  const [selectProject, setSelectProject] = useState(null);

  useEffect(() => {
    async function getProjects() {
      const response = await api.get('projects');

      const data = response.data.map(project => ({
        ...project,
        startDateFormatted: format(
          parseISO(project.date_start),
          "dd'/'MM'/'Y",
          {
            locale: ptBR,
          }
        ),
        endDateFormatted: format(
          parseISO(project.date_end),
          "dd'/'MM'/'Y",
          {
            locale: ptBR,
          }
        ),
      }));

      setProjects(data);
    }

    getProjects();
  }, []);

  useEffect(() => {
    dispatch(getProjectsRequest(search));
  }, [dispatch, search]);

  useEffect(() => {
    setOpenModal(false);
  }, [projects]);

  function handleCloseModal() {
    setOpenModal(false);
  }

  function handleOpenModal(project) {
    setSelectProject(project);
    setOpenModal(true);
  }

  function handleSubmit() {
    const { id } = selectProject;
    dispatch(deleteProjectRequest(id));
    setOpenModal(false);
  }

  return (
    <Container>
      <Header>
        <h1>Gerenciando Projetos</h1>
        <div>
          <Link to="/projects/new">
            <FaPlus size={14} color="#fff" />
            <label>CADASTRAR</label>
          </Link>

          <InputSearch>
            <FaSearch size={18} color="#999" />
            <Input
              name="search"
              /* onChange={e => setSearch(e.target.value)} */
              placeholder="Buscar projeto"
            />
          </InputSearch>
        </div>
      </Header>

      {loading ? (
        <Loading>Carregando...</Loading>
      ) : (
        <Content>
          <section>
            {projects.length === 0 ? (
              <NotFound />
            ) : (
              <table>
                <thead>
                  <tr>
                    <th width="10%">Projeto</th>
                    <th width="5%">Cliente</th>
                    <th width="40%" align="center">Descrição</th>
                    <th width="5%">Valor</th>
                    <th width="10%" align="center">Status</th>
                    <th width="10%">Data Início/Fim</th>
                    <th width="20%">&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  {projects.map(project => (
                    <tr key={project.id}>
                      <td>{project.title}</td>
                      <td></td>
                      <td>{project.description}</td>
                      <td>R${project.price}</td>
                      <td align="center">
                        {project.status}
                      </td>
                      <td>{project.startDateFormatted} a {project.endDateFormatted}</td>
                      <td>
                        <Link to={`/projects/${project.id}/tasks`}>Tarefas</Link>
                        <Link to={`/projects/${project.id}/edit`}><FaEdit size={20} color="#ee4d64" /></Link>
                        <button
                          type="button"
                          onClick={() => handleOpenModal(project)}
                        >
                          <FaTrash size={20} color="#ee4d64" />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            )}
          </section>
        </Content>
      )}
      {/* end loading */}
      {openModal && (
        <Modal>
          <p>
            Deseja deletar o projeto: <strong>{selectProject.title}</strong>
          </p>
          <button type="button" onClick={handleSubmit}>
            Confirmar
          </button>
          <button type="button" onClick={handleCloseModal}>
            Cancelar
          </button>
        </Modal>
      )}
    </Container>
  );
}

export default Dashboard;
