import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';

import SignIn from '~/pages/SignIn';
import SignUp from '~/pages/SignUp';

import Clients from '~/pages/Clients';
import ClientsForm from '~/pages/ClientsForm';

import Projects from '~/pages/Projects';
import ProjectsForm from '~/pages/ProjectsForm';

import Employees from '~/pages/Employees';
import EmployeesForm from '~/pages/EmployeesForm';

import Tasks from '~/pages/Tasks';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={SignIn} />
      <Route path="/register" component={SignUp} />

      <Route path="/dashboard" component={Projects} isPrivate />
      <Route path="/projects/:id/edit" component={ProjectsForm} isPrivate />
      <Route path="/projects/new" component={ProjectsForm} isPrivate />

      <Route path="/projects/:id/tasks" component={Tasks} isPrivate />

      <Route path="/clients" exact component={Clients} isPrivate />
      <Route path="/clients/new" component={ClientsForm} isPrivate />
      <Route path="/clients/:id/edit" component={ClientsForm} isPrivate />

      <Route path="/employees" exact component={Employees} isPrivate />
      <Route
        path="/employees/:id/edit"
        component={EmployeesForm}
        isPrivate
      />
      <Route path="/employees/new" component={EmployeesForm} isPrivate />
    </Switch>
  );
}
