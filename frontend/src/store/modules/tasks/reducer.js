const initialState = {
  data: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case '@task/GET_SUCCESS':
      return {
        ...state,
        data: action.payload.data,
      };

    case '@task/DELETE_REQUEST':
      const { id } = action.payload;
      return {
        ...state,
        data: state.data.filter(task => task.id !== id),
      };

    default:
      return state;
  }
}
