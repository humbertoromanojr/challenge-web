import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import api from '~/services/api';
import history from '~/services/history';
import { getEmployeesSuccess } from './actions';

export function* getEmployees() {
  try {
    const response = yield call(api.get, `users`);

    const data = response.data.map(employee => ({
      ...employee,
    }));

    yield put(getEmployeesSuccess(data));
  } catch (error) {
    toast.error('Falha: Problemas na listagens das matrículas');
  }
}

export function* createEmployee({ payload }) {
  try {
    const { data } = payload;

    yield call(api.post, 'users', data);

    toast.success('Matrícula criada com sucesso');
    history.push('/users');
  } catch (error) {
    toast.error('Falha: Matrícula não foi criada');
  }
}

export function* updateEmployee({ payload }) {
  try {
    const { id, data } = payload;

    yield call(api.put, `users/${id}`, data);

    toast.success('Matrícula atualizada com sucesso');
  } catch (error) {
    toast.error('Falha: Matrícula não foi atualizada');
  }
}

export function* deleteEmployee({ payload }) {
  try {
    const { id } = payload;

    yield call(api.delete, `users/${id}`);

    toast.success('Matrícula deletada com sucesso');
  } catch (error) {
    toast.error('Falha: Matrícula não foi deletada de nosso banco de dados.');
  }
}

export default all([
  takeLatest('@employee/GET_REQUEST', getEmployees),
  takeLatest('@employee/DELETE_REQUEST', deleteEmployee),
  takeLatest('@employee/UPDATE_REQUEST', updateEmployee),
  takeLatest('@employee/CREATE_REQUEST', createEmployee),
]);
