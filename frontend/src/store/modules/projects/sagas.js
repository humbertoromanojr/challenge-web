import { takeLatest, call, put, all } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import api from '~/services/api';
import history from '~/services/history';

import { getProjectsSuccess } from './actions';

export function* getProjects({ payload }) {
  try {

    const response = yield call(api.get, `projects`);

    yield put(getProjectsSuccess(response.data));
  } catch (error) {
    toast.error('Problemas na listagens dos projetos');
  }
}

export function* createProject({ payload }) {
  try {
    const { data } = payload;

    yield call(api.post, `projects`, data);

    toast.success('Projeto criado com sucesso');
  } catch (error) {
    toast.error('Falha: Projeto não foi criado');
  }
}

export function* updateProject({ payload }) {
  try {
    const { id, data } = payload;
    console.log('Sagas -> project ->', data)

    yield call(api.put, `projects/${id}`, data);

    toast.success('Projeto atualizado com sucesso');
    history.push('/dashboard');
  } catch (error) {
    toast.error('Falha: Projeto não foi atualizado');
  }
}

export function* deleteProject({ payload }) {
  try {
    const { id } = payload;

    yield call(api.delete, `projects/${id}`);

    toast.success('Projeto deletado com sucesso');
    history.push('/dashboard');
  } catch (error) {
    toast.error('Falha: Projeto não foi removido');
  }
}

export default all([
  takeLatest('@project/GET_REQUEST', getProjects),
  takeLatest('@project/DELETE_REQUEST', deleteProject),
  takeLatest('@project/UPDATE_REQUEST', updateProject),
  takeLatest('@project/CREATE_REQUEST', createProject),
]);
