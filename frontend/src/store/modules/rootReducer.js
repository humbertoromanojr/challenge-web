import { combineReducers } from 'redux';

import auth from './auth/reducer';
import user from './user/reducer';
import projects from './projects/reducer';
import clients from './clients/reducer';
import employees from './employees/reducer';
import tasks from './tasks/reducer'; 

export default combineReducers({
  auth,
  user,
  projects,
  clients,
  employees,
  tasks
});
