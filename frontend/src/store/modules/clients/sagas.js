import { all, takeLatest, call, put } from 'redux-saga/effects';
import { toast } from 'react-toastify';

import api from '~/services/api';

import { getClientsSuccess } from './actions'

export function* getClients() {
  try {
    const response = yield call(api.get, 'clients');

    yield put(getClientsSuccess(response.data));

  } catch (error) {
    toast.error('Algo não deu certo, problemas na listagens dos clientes');
  }
}

export function* createClient({ payload }) {
  try {
    const { data } = payload;

    yield call(api.post, 'clients', data);

    toast.success('Cliente criado com sucesso');
  } catch (error) {
    toast.error('Algo não deu certo, problemas no cadastro de cliente');
  }
}

export function* updateClient({ payload }) {
  try {
    const { id, data } = payload;

    yield call(api.put, `clients/${id}`, data);

    toast.success('Cliente alterado com sucesso');
  } catch (error) {
    toast.error('Algo não deu certo, problemas na alteração de cliente');
  }
}

export function* deleteClient({ payload }) {
  try {
    const { id } = payload;

    yield call(api.delete, `clients/${id}`);

    toast.success('Cliente deletado com sucesso');
  } catch (error) {
    toast.error('Algo não deu certo, problemas ao deletar o cliente');
  }
}

export default all([
  takeLatest('@client/GET_SUCCESS', getClients),
  takeLatest('@client/CREATE_REQUEST', createClient),
  takeLatest('@client/UPDATE_REQUEST', updateClient),
  takeLatest('@client/DELETE_REQUEST', deleteClient),
]);
