const initialState = {
  data: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case '@client/GET_SUCCESS':
      return {
        ...state,
        data: action.payload.data,
      };

    case '@client/DELETE_REQUEST':
      const { id } = action.payload;
      return {
        ...state,
        data: state.data.filter(client => client.id !== id),
      };

    default:
      return state;
  }
}
